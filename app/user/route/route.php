<?php

namespace app\user\route;

use app\common\middleware\Auth;
use think\facade\Route;

Route::group(function () {
    // 用户相关
    Route::group(function () {
        // 登录
        Route::post('login', 'login/index');
        // 首次授权设置用户信息
        Route::post('setUserInfo', 'index/setUserInfo');
        // 获取用户信息
        Route::post('getUserInfo', 'index/getUserInfo');
        // 绑定手机号
        Route::post('bindMobile', 'index/bindMobile');
        // 获取客服电话
        Route::get('serviceMobile', 'index/getserviceMobile');
        // 修改昵称
        Route::post('editUserName', 'index/editUserName');
    })->prefix('user.');

    Route::group(function () {
        // 获取动态列表
        Route::get('getDeviceTrends', 'index/getDeviceTrends');
        // 修改设备名
        Route::post('editDeviceName', 'index/editDeviceName');
        // 获取指定设备信息
        Route::post('getDevice', 'index/getDeviceInfo');
        // 删除设备
        Route::post('delDevice', 'index/delDevice');
        // 切换模式
        Route::post('editModel', 'index/editDeviceModel');
        // 添加设备
        Route::post('addDevice', 'index/addDevice');
        // 设备列表
        Route::post('deviceList', 'index/deviceList');
        // 获取用户设备数量
        Route::post('getDeviceNum', 'index/getDeviceNum');
    })->prefix('device.');

    Route::group(function () {
        // 所有设备
        Route::post('allDevice', 'index/getAllDevice');
    })->prefix('home.');

    //...
})->middleware(Auth::class);
