<?php

namespace app\user\controller\user;

use app\BaseController;
use app\user\model\Seting;
use app\user\model\User;
use think\Request;

class Index extends BaseController
{
    // 获取用户信息
    public function getUserInfo(Request $request)
    {
        $uid = $request->user_id;
        $userModel = new User();
        $userInfo = $userModel->getFind(['id' => $uid], 'username as nickName,avatar as avatarUrl,mobile');
        if (empty($userInfo)) return success([]);
        if ($userInfo['nickName'] == null && $userInfo['avatarUrl'] == null) {
            $res = [
                'is_empower' => 1
            ];
        } else {
            $res = [
                'is_empower' => 0,
                'data' => $userInfo
            ];
        }
        return success($res);
    }
    // 修改昵称
    public function editUserName(Request $request)
    {
        $username = $request->param()['nickname'];
        $uid = $request->user_id;
        $userModel = new User();
        $userModel->getUpdate(['id' => $uid], [
            'username' => $username,
            'utime' => time()
        ]);
        return success([]);
    }

    // 首次授权设置用户信息
    public function setUserInfo(Request $request)
    {
        $avatar = $request->param()['avatar'];
        $uid = $request->user_id;
        $userModel = new User();
        $userModel->getUpdate(['id' => $uid], [
            'avatar' => $avatar,
            'utime' => time()
        ]);
        return success([]);
    }

    // 绑定手机号-废弃
    public function bindMobile(Request $request)
    {
        $mobile = $request->param()['mobile'];
        $uid = $request->user_id;
        $userModel = new User();
        $userModel->getUpdate(['id' => $uid], [
            'mobile' => $mobile,
            'utime' => time()
        ]);
        return success([]);
    }

    // 获取客服电话
    public function getserviceMobile(Request $request)
    {
        $setModel =  new Seting();
        $mobile = $setModel->getFind(['name' => 'service_mobile']);
        if (empty($mobile)) {
            return fail([], '抱歉，暂无客服电话');
        } else {
            return success($mobile['value']);
        }
    }
}
