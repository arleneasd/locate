<?php

namespace app\user\controller\home;

use app\BaseController;
use app\user\model\Device;
use app\user\model\Devicetrends;
use think\Request;

class Index extends BaseController
{
    // 获取用户所有设备
    public function getAllDevice(Request $request)
    {
        $uid = $request->user_id;
        // $uid = 1;
        $deviceModel = new Device();
        $trendsModel = new Devicetrends();
        $field = ['device_status,lc_device.id,lc_device.device_name,lc_device.device_imei,lc_device.network_card,lc_device.version,lc_device.is_line,lc_device.mode,lc_device_trends.trends_data,lat,lng'];
        $where = [
            ['lc_device.user_id','=',$uid],
            ['lc_device.device_bind_status','=',$deviceModel::DEVICE_BIND_STATUS_YES],
            ['lc_device.datastatus','=',$deviceModel::NORMAL_DATASTATUS],
            ['lc_device_trends.is_new','=',$trendsModel::YES_NEW],
            ['lc_device_trends.datastatus','=',$trendsModel::NORMAL_DATASTATUS]
        ];
        $data = $deviceModel->table('lc_device,lc_device_trends')
        ->where($where)
        ->where('lc_device.id=lc_device_trends.device_id')
        ->field($field)
        ->select()->toArray();
        foreach($data as &$val){
            $locate = json_decode($val['trends_data'],true);
            $loc = explode(',',$locate['gps']);
            $val['locate'] = implode(',',array_reverse($loc));
        }
        return success($data);
    }
}
