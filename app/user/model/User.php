<?php

namespace app\user\model;

use app\common\Helper\FunctionDB;
use think\Model;

class User extends Model
{
    protected $name = 'user';

    /**
     * 查询一条
     * @param array $where 判断条件
     * @param string $field 查询字段
     * @return array
     */
    public function getFind($where, $field = '*')
    {
        return $this->field($field)->where($where)->find();
    }

    /**
     * 插入一条数据
     * @param array $data 插入数据
     * @return int 新插入数据ID
     */
    public function getInsert($data)
    {
        // 插入数据
        return $this->insertGetId($data);
    }

    /**
     * 更新单条数据
     * @param array $where 更新判断条件
     * @param array $data 更新数据
     * @return int 新插入数据ID
     */
    public function getUpdate($where, $data)
    {
        return $this->where($where)->update($data);
    }
}
