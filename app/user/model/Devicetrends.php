<?php

namespace app\user\model;

use think\Model;

class Devicetrends extends Model
{
    protected $name = 'device_trends';

    protected $autoWriteTimestamp = true;
    protected $createTime = 'itime';
    protected $updateTime = 'utime';
    protected $deleteTime = 'dtime';
    // 数据状态：1-正常，0-删除
    const NORMAL_DATASTATUS = 1;
    const DEL_DATASTATUS = 0;

    // 是否最新动态：0-否，1-是
    const NO_NEW = 0;
    const YES_NEW = 1;

    /**
     * 查询一条
     * @param array $where 判断条件
     * @param string $field 查询字段
     * @return array
     */
    public function getFind($where, $field = '*')
    {
        return $this->field($field)->where($where)->find();
    }
}
