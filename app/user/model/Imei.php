<?php

namespace app\user\model;

use think\Model;

class Imei extends Model
{
    protected $name = 'imei';
    
    // 编码状态：0-解绑||删除（可重新绑定），1-已绑定
    const DEVICE_BIND_STATUS_NO = 0;
    const DEVICE_BIND_STATUS_YES = 1;

    // 是否停用：1-正常，2-停用
    const NO_STOP = 1;
    const YES_STOP = 2;
}
