<?php

namespace app\admin\route;

use app\common\middleware\Auth;
use think\facade\Route;

Route::miss(function () {
    return "Hello man!";
});

Route::group(function () {
    Route::group(function () {
        // 管理员登录
        Route::post('login', 'login/adminLogin');
        // 登出
        Route::post('logout', 'login/logout');
    })->prefix('auth.');

    Route::group(function () {
        // 仪表盘
        Route::get('database', 'index/database');
    })->prefix('dashboard.');

    Route::group(function () {
        // 管理员信息
        Route::get('adminInfo', 'index/adminInfo');
        // 管理员列表
        Route::get('adminList', 'index/adminList');
        // 编辑详情信息
        Route::get('adminDetails', 'index/passIdGet');
        // 编辑确认
        Route::post('editAdmin', 'index/confirmEdit');
        // 删除管理员
        Route::post('delAdmin', 'index/delAdmin');
        // 账户状态开关
        Route::post('adminSwitch', 'index/switch');
        // 修改密码
        Route::post('editPwd', 'index/editPwd');
        // 添加管理
        Route::post('addAdmin', 'index/addAdmin');
    })->prefix('admin.');

    Route::group(function () {
        // 用户列表
        Route::get('userList', 'index/userList');
        // 设备列表
        Route::get('device', 'index/deviceList');
        // 账号状态开关
        Route::post('userSwitch', 'index/switch');
        // 解绑设备
        Route::post('unBind', 'index/unBind');
        // 获取设备最后一次上报数据的经纬度
        Route::get('locate', 'index/lastLocate');
    })->prefix('user.');

    Route::group(function () {
        // 设备列表
        Route::get('deviceList', 'index/deviceList');
        // 设备状态开关
        Route::post('deviceSwitch', 'index/switch');
        // 删除设备
        Route::post('delete', 'index/delete');
        // 导入imei编码
        Route::post('import', 'index/importExcel');
        // 导出所有设备
        Route::post('export', 'index/exportExcel');
        // imei编码列表
        Route::post('imeiList', 'index/imeiList');
        // 删除imei编码
        Route::post('imeiDelete', 'index/imeiDelete');
        // 远程认证
        Route::post('remoteAuth', 'auth/remoteAuth');
    })->prefix('device.');

    Route::group(function () {
        // 登录日志列表
        Route::get('loginLog', 'loginlog/loginLog');
        // 删除登陆日志
        Route::post('dellogin', 'loginlog/dellogin');
        // 操作日志列表
        Route::get('opt', 'optlog/optLog');
        // 删除操作日志
        Route::post('delopt', 'optlog/delopt');
    })->prefix('log.');

    Route::group(function () {
        // 获取客服电话信息
        Route::post('getMobile', 'index/getServiceMoblie');
        // 设置客服电话
        Route::post('setMobile', 'index/setMobile');
    })->prefix('set.');
})->middleware(Auth::class);
