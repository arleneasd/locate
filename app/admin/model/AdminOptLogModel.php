<?php

namespace app\admin\model;

use app\common\Helper\FunctionDB;
use think\Model;

class AdminOptLogModel extends Model
{
    protected $fieldAllow = ['itime', 'utime', 'dtime', 'admin_id', 'datastatus', 'ip', 'url'];
    protected $casts = [];
    protected $name = 'admin_opt_log';
    protected $autoWriteTimestamp = true;
    protected $createTime = 'itime';
    protected $updateTime = 'utime';
    protected $deleteTime = 'dtime';

    // 数据状态：1-正常，0-删除
    const NORMAL_DATASTATUS = 1;
    const DEL_DATASTATUS = 0;

    /**
     * 插入一条数据
     * @param array $data 插入数据
     * @return int 新插入数据ID
     */
    public function getInsert($data)
    {
        // 添加数据字段过滤
        $data = FunctionDB::filedFiltrate($data, $this->fieldAllow, $this->casts);
        if (!$data) {
            return 0;
        }
        // 插入数据
        return $this->insert($data);
    }

    /**
     * 查询集合
     * @param array $where 判断条件
     * @param string $field 查询字段
     * @param string $orderSql 排序；sql语句：id desc
     * @param int $currentPage 分页-当前页数；约定参数curpage 当前页码
     * @param int $pageSize 分页-每页显示条数；约定参数pagesize 没有记录数
     * @param int $isPaging 是否分页: 0-否,1-是;
     * @param int $isLimit 是否限制查询: 0-否,1-是;
     * @param int $isMaster 是否查询主库;Boole值;false,true;
     * @return array
     */
    public function getSelect($where, $field = '*', $orderSql = '', $currentPage = 0, $pageSize = 20, $isPaging = 1, $isLimit = 0, $isMaster = false)
    {
        FunctionDB::fieldTurnString($field);
        $dbModel = $this->field($field)->where($where)->order($orderSql);
        if ($isPaging == 1) {
            $dbModel = $dbModel->page($currentPage, $pageSize);
        } elseif ($isLimit == 1) {
            // 不限制查询；默认限制查询1000条
            $dbModel = $dbModel->limit(false);
        }
        return $dbModel->select()->toArray();
    }

    /**
     * 查询总数
     * @param array $where 判断条件
     * @param array $isMaster 是否查询主库;Boole值;false,true;
     * @return int
     */
    public function getCount($where)
    {
        return $this->where($where)->count();
    }

    /**
     * 更新单条数据
     * @param array $where 更新判断条件
     * @param array $data 更新数据
     * @return int 新插入数据ID
     */
    public function getUpdate($where, $data)
    {
        // 添加数据字段过滤
        $data = FunctionDB::filedFiltrate($data, $this->fieldAllow, $this->casts);
        if (!$data) {
            return 0;
        }
        return $this->where($where)->update($data);
    }
}
