<?php

namespace app\admin\model;

use app\common\Helper\FunctionDB;
use think\Model;

class DeviceTrendsModel extends Model
{
    protected $name = 'device_trends';
    protected $fieldAllow = ['device_id', 'itime', 'utime', 'dtime', 'datastatus', 'trends_data', 'addres_json', 'address', 'is_new'];
    protected $casts = ['itime' => 'integer', 'utime' => 'integer', 'dtime' => 'integer'];
    protected $autoWriteTimestamp = true;
    protected $createTime = 'itime';
    protected $updateTime = 'utime';
    protected $deleteTime = 'dtime';

    // 数据状态：1-正常，0-删除
    const NORMAL_DATASTATUS = 1;
    const DEL_DATASTATUS = 0;

    // 是否最新动态：0-否，1-是
    const NO_NEW = 0;
    const YES_NEW = 1;

    /**
     * 更新单条数据
     * @param array $where 更新判断条件
     * @param array $data 更新数据
     * @return int 新插入数据ID
     */
    public function getUpdate($where, $data)
    {
        // 添加数据字段过滤
        $data = FunctionDB::filedFiltrate($data, $this->fieldAllow, $this->casts);
        if (!$data) {
            return 0;
        }
        return $this->where($where)->update($data);
    }

    /**
     * 查询一条
     * @param array $where 判断条件
     * @param string $field 查询字段
     * @return array
     */
    public function getFind($where, $field = '*')
    {
        FunctionDB::fieldTurnString($field);
        return $this->field($field)->where($where)->find();
    }

    /**
     * 查询集合
     * @param array $where 判断条件
     * @param string $field 查询字段
     * @param string $orderSql 排序；sql语句：id desc
     * @param int $currentPage 分页-当前页数；约定参数curpage 当前页码
     * @param int $pageSize 分页-每页显示条数；约定参数pagesize 没有记录数
     * @param int $isPaging 是否分页: 0-否,1-是;
     * @param int $isLimit 是否限制查询: 0-否,1-是;
     * @param int $isMaster 是否查询主库;Boole值;false,true;
     * @return array
     */
    public function getSelect($where, $field = '*', $orderSql = '', $currentPage = 0, $pageSize = 20, $isPaging = 1, $isLimit = 0, $isMaster = false)
    {
        FunctionDB::fieldTurnString($field);
        $dbModel = $this->field($field)->where($where)->order($orderSql);
        if ($isPaging == 1) {
            $dbModel = $dbModel->page($currentPage, $pageSize);
        } elseif ($isLimit == 1) {
            // 不限制查询；默认限制查询1000条
            $dbModel = $dbModel->limit(false);
        }
        return $dbModel->select()->toArray();
    }

    // 根据设备ID获取地址最后一次上报地址
    public function getAddressMore($deviceIds)
    {
        return $this->whereIn('device_id', $deviceIds)->field('device_id,address')->select()->toArray();
    }

    /**
     * 插入一条数据
     * @param array $data 插入数据
     * @return int 新插入数据ID
     */
    public function getInsert($data)
    {
        // 添加数据字段过滤
        $data = FunctionDB::filedFiltrate($data, $this->fieldAllow, $this->casts);
        if (!$data) {
            return 0;
        }
        // 插入数据
        return $this->insert($data);
    }

    // 清除之前的标记
    public function clearflags($imei)
    {
        return $this->where('device_id', $imei)->where('is_new', 1)->update(['is_new' => 0]);
    }
}
