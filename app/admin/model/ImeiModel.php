<?php

namespace app\admin\model;

use app\common\Helper\FunctionDB;
use think\Model;

class ImeiModel extends Model
{
    protected $name = 'imei';
    // 允许更新字段
    protected $fieldAllow = ['itime', 'utime', 'imei', 'status'];

    // 编码状态：0-解绑||删除（可重新绑定），1-已绑定
    const DEVICE_BIND_STATUS_NO = 0;
    const DEVICE_BIND_STATUS_YES = 1;

    // 是否停用：1-正常，2-停用
    const NO_STOP = 1;
    const YES_STOP = 2;

    /**
     * 查询一条
     * @param array $where 判断条件
     * @param string $field 查询字段
     * @return array
     */
    public function getFind($where, $field = '*')
    {
        FunctionDB::fieldTurnString($field);
        return $this->field($field)->where($where)->find();
    }

    /**
     * 更新单条数据
     * @param array $where 更新判断条件
     * @param array $data 更新数据
     * @return int 新插入数据ID
     */
    public function getUpdate($where, $data)
    {
        // 添加数据字段过滤
        $data = FunctionDB::filedFiltrate($data, $this->fieldAllow, $this->casts);
        if (!$data) {
            return 0;
        }
        return $this->where($where)->update($data);
    }

    /**
     * 插入多条数据
     * @param array $data 插入数据二维数组
     * @return
     */
    public function getInsertAll($data)
    {
        // 添加数据字段过滤
        $data = FunctionDB::filedFiltrate($data, $this->fieldAllow, $this->casts);
        if (!$data) {
            return 0;
        }
        // 插入数据
        return $this->insertAll($data);
    }

    // 查询imei列表
    public function getSelect($where, $currentPage = 0, $pageSize = 20, $order = "id desc")
    {
        $dbModel = $this->where($where)->order($order);
        $dbModel = $dbModel->page($currentPage, $pageSize);
        $list =  $dbModel->select()->toArray();
        $count = $this->where($where)->count();
        return ['list' => $list, 'count' => $count];
    }
    // 删除imei编码
    public function imeiDelete($where)
    {
        return $this->where($where)->delete();
    }
}
