<?php

namespace app\admin\model;

use app\common\Helper\FunctionDB;
use think\Model;

class DeviceModel extends Model
{
    protected $name = 'device';
    // 允许更新字段
    protected $fieldAllow = [
        'itime', 'utime', 'dtime', 'user_id', 'device_name', 'device_imei', 'datastatus', 'network_card', 'version', 'on_line_first_time', 'is_line', 'device_bind_status', 'device_bind_way', 'cable', 'temp', 'device_status', 'mode', 'notes', 'lng', 'lat', 'on_line_newly_time', 'off_line_time'
    ];

    protected $autoWriteTimestamp = true;
    protected $createTime = 'itime';
    protected $updateTime = 'utime';
    protected $deleteTime = 'dtime';
    // 数据状态：1-正常，0-删除
    const NORMAL_DATASTATUS = 1;
    const DEL_DATASTATUS = 0;

    // 设备在线状态：1-在线，2-离线
    const DEVICE_ONLINE = 1;
    const DEVICE_OFFLINE = 2;

    // 设备状态：1-正常，2-停用
    const DEVICE_STATUS_NORMAL = 1;
    const DEVICE_STATUS_STOP = 2;

    // 设备绑定状态：0-未绑定，1-已绑定
    const DEVICE_BIND_STATUS_NO = 0;
    const DEVICE_BIND_STATUS_YES = 1;

    // 设备绑定方式：2-手机号，1-openid
    const BINDING_WAY_MOBILE = 2;
    const BINDING_WAY_OPENID = 1;

    // 设备状态
    public function getBinDingStatus()
    {
        return [
            '1' => '启用',
            '2' => '停用'
        ];
    }
    // 设备绑方式
    public function getBinDingWay()
    {
        return [
            '0' => '--',
            '2' => '手机号',
            '1' => '(微信标识openid)',
        ];
    }
    // 统计用户设备状态
    public function getDeviceCount($where)
    {
        return $this->where($where)->count();
    }

    // 获取集合
    public function getSelect($where, $field = '*', $orderSql = '', $currentPage = 0, $pageSize = 20, $isPaging = 1, $isLimit = 0, $isMaster = false)
    {
        FunctionDB::fieldTurnString($field);
        $dbModel = $this->field($field)->where($where)->order($orderSql);
        if ($isPaging == 1) {
            $dbModel = $dbModel->page($currentPage, $pageSize);
        } elseif ($isLimit == 1) {
            // 不限制查询；默认限制查询1000条
            $dbModel = $dbModel->limit(false);
        }
        return $dbModel->select()->toArray();
    }

    /**
     * 更新单条数据
     * @param array $where 更新判断条件
     * @param array $data 更新数据
     * @return int 新插入数据ID
     */
    public function getUpdate($where, $data)
    {
        // 添加数据字段过滤
        $data = FunctionDB::filedFiltrate($data, $this->fieldAllow, $this->casts);
        if (!$data) {
            return 0;
        }
        return $this->where($where)->update($data);
    }

    /**
     * 查询总数
     * @param array $where 判断条件
     * @param array $isMaster 是否查询主库;Boole值;false,true;
     * @return int
     */
    public function getCount($where)
    {
        return $this->where($where)->count();
    }

    // 更新gps数据
    public function updataGps($imei, $gps, $v, $t)
    {
        // 更新gps、电量、温度
        $this->where("device_imei", $imei)->where("datastatus", 1)->where("device_status", 1)->where("device_bind_status", 1)->update(['lat' => $gps[0], 'lng' => $gps[1], 'cable' => $v, 'temp' => $t]);
        // imei查id
        $id = $this->where("device_imei", $imei)->where("datastatus", 1)->where("device_status", 1)->where("device_bind_status", 1)->value('id');
        return $id;
    }
    // 查找设备
    public function checkDevice($imei)
    {
        return $this->where("device_imei", $imei)->where("device_status", 1)->where("device_bind_status", 1)->field("device_imei")->find();
    }

    // 获取用户uid
    public function getUid($id)
    {
        if (empty($id)) {
            return '';
        }
        return $this->where('id', $id)->value('user_id');
    }
}
