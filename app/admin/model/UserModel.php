<?php

namespace app\admin\model;

use app\common\Helper\FunctionDB;
use think\Model;

class UserModel extends Model
{
    protected $name = 'user';
    // 允许更新字段
    protected $fieldAllow = ['itime', 'utime', 'dtime', 'username', 'mobile', 'avatar', 'openid', 'status', 'device_num', 'datastatus'];
    /**
     * 查询集合
     * @param array $where 判断条件
     * @param string $field 查询字段
     * @param string $orderSql 排序；sql语句：id desc
     * @param int $currentPage 分页-当前页数；约定参数curpage 当前页码
     * @param int $pageSize 分页-每页显示条数；约定参数pagesize 没有记录数
     * @param int $isPaging 是否分页: 0-否,1-是;
     * @param int $isLimit 是否限制查询: 0-否,1-是;
     * @param int $isMaster 是否查询主库;Boole值;false,true;
     * @return array
     */
    public function getSelect($where, $field = '*', $orderSql = '', $currentPage = 0, $pageSize = 20, $isPaging = 1, $isLimit = 0, $isMaster = false)
    {
        FunctionDB::fieldTurnString($field);
        $dbModel = $this->field($field)->where($where)->order($orderSql);
        if ($isPaging == 1) {
            $dbModel = $dbModel->page($currentPage, $pageSize);
        } elseif ($isLimit == 1) {
            // 不限制查询；默认限制查询1000条
            $dbModel = $dbModel->limit(false);
        }
        return $dbModel->select()->toArray();
    }

    /**
     * 查询总数
     * @param array $where 判断条件
     * @param array $isMaster 是否查询主库;Boole值;false,true;
     * @return int
     */
    public function getCount($where)
    {
        return $this->where($where)->count();
    }

    /**
     * 更新单条数据
     * @param array $where 更新判断条件
     * @param array $data 更新数据
     * @return int 新插入数据ID
     */
    public function getUpdate($where, $data)
    {
        // 添加数据字段过滤
        $data = FunctionDB::filedFiltrate($data, $this->fieldAllow, $this->casts);
        if (!$data) {
            return 0;
        }
        return $this->where($where)->update($data);
    }
}
