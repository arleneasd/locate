<?php

namespace app\admin\model;

use app\common\Helper\FunctionDB;
use think\Model;

class AdminModel extends Model
{
    protected $name = 'admin';
    // 允许更新字段
    protected $fieldAllow = ['itime', 'datastatus', 'utime', 'dtime', 'admin_status', 'username', 'mobile', 'encrypt_mobile', 'notes', 'password', 'token', 'is_super', 'headimg', 'salt'];
    // 转换的属性
    protected $casts = [];
    protected $autoWriteTimestamp = true;
    protected $createTime = 'itime';
    protected $updateTime = 'utime';
    protected $deleteTime = 'dtime';

    // 超管状态：0-否，1-是
    const NO_SUPER = 0;
    const YES_SUPER = 1;

    /**
     * 查询一条
     * @param array $where 判断条件
     * @param string $field 查询字段
     * @return array
     */
    public function getFind($where, $field = '*')
    {
        FunctionDB::fieldTurnString($field);
        return $this->field($field)->where($where)->find();
    }

    /**
     * 更新单条数据
     * @param array $where 更新判断条件
     * @param array $data 更新数据
     * @return int 新插入数据ID
     */
    public function getUpdate($where, $data)
    {
        // 添加数据字段过滤
        $data = FunctionDB::filedFiltrate($data, $this->fieldAllow, $this->casts);
        if (!$data) {
            return 0;
        }
        return $this->where($where)->update($data);
    }

    /**
     * 查询集合
     * @param array $where 判断条件
     * @param string $field 查询字段
     * @param string $orderSql 排序；sql语句：id desc
     * @param int $currentPage 分页-当前页数；约定参数curpage 当前页码
     * @param int $pageSize 分页-每页显示条数；约定参数pagesize 没有记录数
     * @param int $isPaging 是否分页: 0-否,1-是;
     * @param int $isLimit 是否限制查询: 0-否,1-是;
     * @param int $isMaster 是否查询主库;Boole值;false,true;
     * @return array
     */
    public function getSelect($where, $field = '*', $orderSql = '', $currentPage = 0, $pageSize = 20, $isPaging = 1, $isLimit = 0, $isMaster = false)
    {
        FunctionDB::fieldTurnString($field);
        $dbModel = $this->field($field)->where($where)->order($orderSql);
        if ($isPaging == 1) {
            $dbModel = $dbModel->page($currentPage, $pageSize);
        } elseif ($isLimit == 1) {
            // 不限制查询；默认限制查询1000条
            $dbModel = $dbModel->limit(false);
        }
        return $dbModel->select()->toArray();
    }

    /**
     * 查询总数
     * @param array $where 判断条件
     * @param array $isMaster 是否查询主库;Boole值;false,true;
     * @return int
     */
    public function getCount($where)
    {
        return $this->where($where)->count();
    }

    /**
     * 插入一条数据
     * @param array $data 插入数据
     * @return int 新插入数据ID
     */
    public function getInsert($data)
    {
        // 添加数据字段过滤
        $data = FunctionDB::filedFiltrate($data, $this->fieldAllow, $this->casts);
        if (!$data) {
            return 0;
        }
        // 插入数据
        return $this->insert($data);
    }

    /**
     * 插入多条数据
     * @param array $data 插入数据二维数组
     * @return
     */
    public function getInsertAll($data)
    {
        // 添加数据字段过滤
        $data = FunctionDB::filedFiltrate($data, $this->fieldAllow, $this->casts);
        if (!$data) {
            return 0;
        }
        // 插入数据
        return $this->insertAll($data);
    }
}
