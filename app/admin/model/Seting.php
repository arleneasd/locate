<?php

namespace app\admin\model;

use think\Model;

class Seting extends Model
{
    protected $name = 'seting';

    protected $autoWriteTimestamp = true;
    protected $createTime = 'itime';
    protected $updateTime = 'utime';
    protected $deleteTime = 'dtime';

    /**
     * 更新单条数据
     * @param array $where 更新判断条件
     * @param array $data 更新数据
     * @return int 新插入数据ID
     */
    public function getUpdate($where, $data)
    {
        return $this->where($where)->update($data);
    }

    /**
     * 查询集合
     * @param array $where 判断条件
     * @param string $field 查询字段
     * @param string $orderSql 排序；sql语句：id desc
     * @param int $currentPage 分页-当前页数；约定参数curpage 当前页码
     * @param int $pageSize 分页-每页显示条数；约定参数pagesize 没有记录数
     * @param int $isPaging 是否分页: 0-否,1-是;
     * @param int $isLimit 是否限制查询: 0-否,1-是;
     * @param int $isMaster 是否查询主库;Boole值;false,true;
     * @return array
     */
    public function getSelect($where, $field = '*', $orderSql = '', $currentPage = 0, $pageSize = 20, $isPaging = 1, $isLimit = 0, $isMaster = false)
    {
        $dbModel = $this->field($field)->where($where)->order($orderSql);
        if ($isPaging == 1) {
            $dbModel = $dbModel->page($currentPage, $pageSize);
        } elseif ($isLimit == 1) {
            // 不限制查询；默认限制查询1000条
            $dbModel = $dbModel->limit(false);
        }
        return $dbModel->select()->toArray();
    }

    /**
     * 查询一条
     * @param array $where 判断条件
     * @param string $field 查询字段
     * @return array
     */
    public function getFind($where, $field = '*')
    {
        return $this->field($field)->where($where)->find();
    }

    /**
     * 插入一条数据
     * @param array $data 插入数据
     * @return int 新插入数据ID
     */
    public function getInsert($data)
    {
        // 插入数据
        return $this->insert($data);
    }
}
