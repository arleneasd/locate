<?php

namespace app\admin\controller\user;

use app\admin\model\DeviceModel;
use app\admin\model\DeviceTrendsModel;
use app\admin\model\ImeiModel;
use app\admin\model\UserModel;
use app\common\controller\Backend;
use app\common\Helper\AppTools;
use think\Request;

class Index extends Backend
{
    // 用户列表
    public function userList(Request $request)
    {
        $param = $request->param();
        $data = json_decode($param['data'], true);
        $userModel = new UserModel();
        $deviceModel = new DeviceModel();
        $searchMobile = $data['mobile'];
        $searchName = $data['name'];
        $currpage = $data['page'];
        $pagesize = $data['size'];
        $where = [];
        if (!empty($searchMobile)) $where[] = ['mobile', 'like', "%{$searchMobile}%"];
        if (!empty($searchName)) $where[] = ['username', 'like', "%{$searchName}%"];
        $field = ['id,username,mobile,device_num,itime,status'];
        $listData = $userModel->getSelect($where, $field);
        foreach ($listData as &$val) {
            $val['on_line'] = $deviceModel->getDeviceCount([
                ['user_id', '=', $val['id']],
                ['is_line', '=', $deviceModel::DEVICE_ONLINE]
            ]);
            $val['off_line'] = $deviceModel->getDeviceCount([
                ['user_id', '=', $val['id']],
                ['is_line', '=', $deviceModel::DEVICE_OFFLINE]
            ]);
            $val['mobile'] = substr($val['mobile'], 0, 3) . '****' . substr($val['mobile'], 7, 4);
            $val['notes'] = $val['notes'] ?? '--';
            $val['itime'] = date('Y-m-d H:i:s', $val['itime']);
        }
        if (count($listData) < 1) return success([]);
        $count = $userModel->getCount($where);
        $list = AppTools::morePage($listData, $count, $currpage, $pagesize);
        return success($list);
    }

    // 设备列表
    public function deviceList(Request $request)
    {
        $param = $request->param();
        $data = json_decode($param['data'], true);
        $id = $data['id'];
        if (empty($id)) return fail([], 'id标识参数错误');
        // 用户设备列表
        $deviceModel = new DeviceModel();
        // 设备定位数据
        $deviceTrendsModel = new DeviceTrendsModel();
        // 设备imei列表
        $field = ['id as device_id,user_id,device_name,device_imei,network_card,version,is_line,device_status,device_bind_status,cable,temp,mode,lng,lat'];
        // 获取用户设备列表
        $listData = $deviceModel->getSelect([['user_id', '=', $id], ['datastatus', '=', $deviceModel::NORMAL_DATASTATUS], ['device_bind_status', '=', $deviceModel::DEVICE_BIND_STATUS_YES]], $field);
        // 弃用
        // $device_ids = array_column($listData, 'device_id');
        // 弃用
        // $locate = $deviceTrendsModel->getSelect([['device_id', 'in', $device_ids], ['is_new', '=', $deviceTrendsModel::YES_NEW]], 'device_id,trends_data,address');
        return success($listData);
        // 弃用
        // $keyval = array_column($locate, null, 'device_id');
        // foreach ($listData as &$val) {
        //     $locate = json_decode($keyval[$val['device_id']]['trends_data'], true);
        //     $loc = explode(',', $locate['locate']);
        //     $val['lng'] = $loc[0];
        //     $val['lat'] = $loc[1];
        //     $val['address'] = $keyval[$val['device_id']]['address'];
        // }
        // return success($listData);
    }

    // 账号状态开关
    public function switch(Request $request)
    {
        $param = $request->param();
        $id = $param['id'];
        $switch = $param['switch'];
        if (empty($id)) return fail([], 'id标识参数错误');
        if (empty($switch)) return fail([], 'switch标识参数错误');
        $userModel = new UserModel();
        $userModel->getUpdate(['id' => $id], ['status' => $switch, 'utime' => time()]);
        return success([]);
    }

    // 解绑设备
    public function unBind(Request $request)
    {
        $param = $request->param();
        $id = $param['device_id'];
        $deviceModel = new DeviceModel();
        $deviceTrendsModel = new DeviceTrendsModel();
        $imeiModel = new ImeiModel();
        $unBinDing = $deviceModel->getUpdate(['id' => $id], ['device_bind_status' => $deviceModel::DEVICE_BIND_STATUS_NO]);
        if ($unBinDing) {
            $deviceTrendsModel->getUpdate(['device_id' => $id], ['datastatus' => $deviceTrendsModel::DEL_DATASTATUS]);
            upImei($id, 'status', $imeiModel::DEVICE_BIND_STATUS_NO);
            return success([]);
        }
        return fail([], '(UPDATE ERROR)更新错误,设备或已解绑');
    }

    // 获取设备最后一次上报数据的经纬度
    public function lastLocate(Request $request)
    {
        $param = $request->param();
        $data = json_decode($param['data'], true);
        $id = $data['device_id'];
        $deviceTrendsModel = new DeviceTrendsModel();
        $locate = $deviceTrendsModel->where(['device_id' => $id, 'is_new' => $deviceTrendsModel::YES_NEW])->order("id","DESC")->find();
        if ($locate) {
            return success($locate);
        } else {
            return fail([], '设备没有上报新数据');
        }
    }
}
