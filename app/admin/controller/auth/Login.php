<?php

namespace app\admin\controller\auth;

use app\admin\model\AdminLoginLogModel;
use app\admin\model\AdminModel;
use app\common\controller\Backend;
use think\Request;

class Login extends Backend
{
    // 管理员登录
    public function adminLogin(Request $request)
    {
        $param      = $request->param();
        $username   = $param['username'];
        $password   = $param['password'];
        $adminModel = new AdminModel();
        $adminLoginLogModel = new AdminLoginLogModel();
        $findAdmin = $adminModel->getFind(['username' => $username], 'admin_status,id,salt,password,mobile,username,headimg');
        if (empty($findAdmin)) return fail([], '用户名密码错误');
        if ($findAdmin['admin_status'] == 2) return fail([], '状态异常，无法登录');
        $salt = $findAdmin['salt'];
        $encryptPwd = encryptPwd($salt, $password);
        if ($encryptPwd != $findAdmin['password']) return fail([], '用户名密码错误');
        $token = getToken('admin_id', $findAdmin['id']);
        $res = [
            // 'admin_id' => $findAdmin['id'],
            // 'mobile' => substr($findAdmin['mobile'] , 0 , 3).'****'.substr($findAdmin['mobile'] , 7 , 4),
            // 'admin_name' => $findAdmin['username'],
            // 'admin_avatar' => $findAdmin['headimg'],
            'token' => $token,
            'rule' => []
        ];
        $adminModel->getUpdate(['id' => $findAdmin['id']], ['token' => $token]);
        $adminLoginLogModel->getInsert([
            'itime' => time(),
            'admin_id' => $findAdmin['id'],
            'ip' => $_SERVER['REMOTE_ADDR']
        ]);
        return success($res);
    }

    // 登出
    public function logout(Request $request)
    {
        return success();
    }
}
