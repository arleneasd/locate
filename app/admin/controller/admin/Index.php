<?php

namespace app\admin\controller\admin;

use app\admin\model\AdminModel;
use app\common\controller\Backend;
use app\common\Helper\AppTools;
use think\Request;

class Index extends Backend
{
    // 管理员信息
    public function adminInfo(Request $request)
    {
        $param = $request->param();
        $token = $param['token'];
        if (empty($token)) return fail([], 'token标识参数错误');
        $adminModel = new AdminModel();
        $findAdmin =  $adminModel->getFind(['token' => $token]);
        $res = [
            'admin_id' => $findAdmin['id'],
            'mobile' => substr($findAdmin['mobile'], 0, 3) . '****' . substr($findAdmin['mobile'], 7, 4),
            'name' => $findAdmin['username'],
            'avatar' => $findAdmin['headimg'],
        ];
        return success($res);
    }

    // 管理员列表
    public function adminList(Request $request)
    {
        $param = $request->param();
        $data = json_decode($param['data'], true);
        $adminModel = new AdminModel();
        $searchMobile = $data['mobile'];
        $searchName = $data['name'];
        $currpage = $data['page'];
        $pagesize = $data['size'];
        $where = [];
        $where['datastatus'] = 1;
        if (!empty($searchMobile)) $where[] = ['mobile', 'like', "%{$searchMobile}%"];
        if (!empty($searchName)) $where[] = ['username', 'like', "%{$searchName}%"];
        $listData = $adminModel->getSelect($where, 'id,username,mobile,admin_status,notes,itime,is_super', '', $currpage, $pagesize);
        if (count($listData) < 1) return success([]);
        foreach ($listData as $val) {
            $val['mobile'] = substr($val['mobile'], 0, 3) . '****' . substr($val['mobile'], 7, 4);
            $val['notes'] = $val['notes'] ?? '--';
        }
        $count = $adminModel->getCount($where);
        $list = AppTools::morePage($listData, $count, $currpage, $pagesize);
        return success($list);
    }

    // 编辑详情信息
    public function passIdGet(Request $request)
    {
        $param = $request->param();
        $data = json_decode($param['data'], true);
        $adminModel = new AdminModel();
        $admin_id = $data['id'];
        if (empty($admin_id)) return fail([], 'id标识参数错误');
        $res = $adminModel->getFind(['id' => $admin_id], 'id,username,mobile,admin_status,notes,itime,is_super');
        return success($res);
    }

    // 编辑确认
    public function confirmEdit(Request $request)
    {
        $param = $request->param();
        $id = $param['data']['id'];
        if (empty($id)) return fail([], 'id标识参数错误');
        $adminModel = new AdminModel();
        $data['utime'] = time();
        $param['data']['itime'] = strtotime($param['data']['itime']);
        $adminModel->getUpdate(['id' => $id], $param['data']);
        return success([]);
    }

    // 删除管理员
    public function delAdmin(Request $request)
    {
        $param = $request->param();
        $id = $param['id'];
        if (empty($id)) return fail([], 'id标识参数错误');
        if ($id == 1) return fail([], 'admin账户不能删除');
        $adminModel = new AdminModel();
        $adminModel->getUpdate(['id' => $id], ['datastatus' => 2, 'utime' => time(), 'dtime' => time()]);
        return success([]);
    }

    // 账户状态开关
    public function switch(Request $request)
    {
        $param = $request->param();
        $id = $param['id'];
        $switch = $param['switch'];
        if (empty($id)) return fail([], 'id标识参数错误');
        if (empty($switch)) return fail([], 'switch标识参数错误');
        $adminModel = new AdminModel();
        $adminModel->getUpdate(['id' => $id], ['admin_status' => $switch, 'utime' => time()]);
        return success([]);
    }

    // 修改密码
    public function editPwd(Request $request)
    {
        $param = $request->param();
        $id = $param['id'];
        $pwd = $param['pwd'];
        if (empty($id)) return fail([], 'id标识参数错误');
        if (empty($pwd)) return fail([], 'pwd标识参数错误');
        $adminModel = new AdminModel();
        $salt = $adminModel->getFind(['id' => $id], 'salt')['salt'];
        $encryptPwd = encryptPwd($salt, $pwd);
        $adminModel->getUpdate(['id' => $id], ['password' => $encryptPwd, 'utime' => time()]);
        return success([]);
    }

    // 添加管理
    public function addAdmin(Request $request)
    {
        $param = $request->param();
        $data = $param['data'];
        if (empty($data)) return fail([], 'data标识参数错误');
        $adminModel = new AdminModel();
        $pwd = $data['password'];
        $mobile = $data['mobile'];
        $salt = getRandStr('big_letter_num', 8);
        $data['password'] = encryptPwd($salt, $pwd);
        $data['encrypt_mobile'] = encryptMobile($salt, $mobile);
        $data['salt'] = $salt;
        $data['itime'] = time();
        $adminModel->getInsert($data);
        return success([]);
    }
}
