<?php

namespace app\admin\controller\set;

use app\admin\model\Seting;
use app\common\controller\Backend;
use think\Request;

class Index extends Backend
{
    // 获取客服电话信息
    public function getServiceMoblie(Request $request)
    {
        $setModel = new Seting();
        $info = $setModel->getFind(['name' => 'service_mobile']);
        return success($info['value']);
    }

    // 设置客服电话
    public function setMobile(Request $request)
    {
        $setModel = new Seting();
        $mobile = $request->param()['mobile'];
        $setModel->getUpdate(['name' => 'service_mobile'], ['value' => $mobile]);
        return success([], '操作成功');
    }

    // 新增配置项
    // public function addSet(Request $request)
    // {
    //     $param = $request->param();
    //     $setModel = new Seting();
    //     $setName = $param['name'];
    //     $setValue = $param['value'];
    //     $setNote = $param['note'];
    //     if (empty($setName) || empty($setValue) || empty($setNote)) {
    //         return fail([], '配置参数不完整');
    //     }
    //     $setModel->getInsert([
    //         'name' => $setName,
    //         'value' => $setValue,
    //         'note' => $setNote,
    //     ]);
    //     return success([], '添加成功');
    // }
}
