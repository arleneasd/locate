<?php
declare (strict_types = 1);

namespace app\admin\controller\device;

use app\admin\model\DeviceModel;
use think\Request;

class auth
{
    /**
     * 验证链接设备 --POST备选方式
     *
     * @return \think\Response
     */
    public function index(Request $request)
    {
        $imei = $request->post()['imei'];
        $device = new DeviceModel();
        $callback = $device->checkDevice($imei);
        if (empty($callback)) {
            $data['result'] = "ignore";
            return json($data);
        } else {
            $data['result'] = "allow";
            return json($data);
        }
    }
}
