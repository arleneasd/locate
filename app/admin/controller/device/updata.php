<?php

declare(strict_types=1);

namespace app\admin\controller\device;

use app\admin\model\DeviceModel;
use app\admin\model\DeviceTrendsModel;
use app\user\model\Device;
use think\Request;

class updata
{
    /**
     * 数据更新
     *
     * @return \think\Response
     * @param string imei  设备imei
     * @param string gps  设备gps
     * @param int v  设备电量
     * @param int t  设备温度
     */
    public function index($data, $imei, $gps, $v, $t)
    {
        // 把gps从逗号分割成两部分
        $gps = explode(',', $gps);
        // 根据imei更新设备信息
        $device = new DeviceModel();
        $deviceid = $device->updataGps($imei, $gps, $v, $t);
        if (!empty($deviceid)) {
            // 添加记录信息
            $trends = new DeviceTrendsModel();
            // 清除之前的新数据的标记位
            $trends->clearflags($deviceid);
            // 写入新数据
            // 添加设备
            $insertData = [
                'itime' => time(),
                'device_id' => $deviceid,
                'datastatus' => 1,
                'trends_data' => $data,
                'is_new' => 1
            ];
            $trends->getInsert($insertData);
            print("Client数据更新[IMEI:" . $imei . "]\r\n");
        } else {
            print_r('数据更新失败', $data,date("Y-m-d H:i:s"));
        }
    }
}
