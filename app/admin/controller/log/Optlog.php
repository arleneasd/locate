<?php

namespace app\admin\controller\log;

use app\admin\model\AdminModel;
use app\admin\model\AdminOptLogModel;
use app\common\controller\Backend;
use app\common\Helper\AppTools;
use think\Request;

class Optlog extends Backend
{
    // 操作日志列表
    public function optLog(Request $request)
    {
        $param = $request->param();
        $data = json_decode($param['data'], true);
        $currpage = $data['page'];
        $pagesize = $data['size'];
        $optModel = new AdminOptLogModel();
        $adminModel = new AdminModel();
        $where = ['datastatus' => $optModel::NORMAL_DATASTATUS];
        $logList = $optModel->getSelect($where, 'id,itime,admin_id,ip,url', '', $currpage, $pagesize);
        $adminIds = array_column($logList, 'admin_id');
        $adminInfo = $adminModel->getSelect([['id', 'in', $adminIds]], 'id as admin_id,username');
        $adminName = array_column($adminInfo, null, 'admin_id');
        foreach ($logList as &$val) {
            $val['admin_name'] = $adminName[$val['admin_id']]['username'];
            $urls = explode('?', $val['url']);
            $val['url'] = $urls[0];
        }
        $count = $optModel->getCount($where);
        $list = AppTools::morePage($logList, $count, $currpage, $pagesize);
        return success($list);
    }


    // 删除日志
    public function delopt(Request $request)
    {
        $param = $request->param();
        $id = $param['id'];
        $optModel = new AdminOptLogModel();
        $optModel->getUpdate(['id' => $id], ['datastatus' => $optModel::DEL_DATASTATUS]);
        return success([]);
    }
}
