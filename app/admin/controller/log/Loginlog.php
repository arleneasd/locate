<?php

namespace app\admin\controller\log;

use app\admin\model\AdminLoginLogModel;
use app\admin\model\AdminModel;
use app\common\controller\Backend;
use app\common\Helper\AppTools;
use think\Request;

class Loginlog extends Backend
{
    // 登录日志列表
    public function loginLog(Request $request)
    {
        $param = $request->param();
        $data = json_decode($param['data'], true);
        $currpage = $data['page'];
        $pagesize = $data['size'];
        $loginLogModel = new AdminLoginLogModel();
        $adminModel = new AdminModel();
        $where = ['datastatus' => $loginLogModel::NORMAL_DATASTATUS];
        $logList = $loginLogModel->getSelect($where, 'id,itime,admin_id,ip', '', $currpage, $pagesize);
        $adminIds = array_column($logList, 'admin_id');
        $adminInfo = $adminModel->getSelect([['id', 'in', $adminIds]], 'id as admin_id,username');
        $adminName = array_column($adminInfo, null, 'admin_id');
        foreach ($logList as &$val) {
            $val['admin_name'] = $adminName[$val['admin_id']]['username'];
        }
        $count = $loginLogModel->getCount($where);
        $list = AppTools::morePage($logList, $count, $currpage, $pagesize);
        return success($list);
    }

    // 删除日志
    public function dellogin(Request $request)
    {
        $param = $request->param();
        $id = $param['id'];
        $loginLogModel = new AdminLoginLogModel();
        $loginLogModel->getUpdate(['id' => $id], ['datastatus' => $loginLogModel::DEL_DATASTATUS]);
        return success([]);
    }
}
