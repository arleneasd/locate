<?php

declare(strict_types=1);

namespace app\common\exception;


class Message
{
    //自定义状态

    const OK                    = '操作成功';
    const BAD_REQUEST           = '服务端异常';
    const INTERNAL_SERVER_ERROR = '服务器错误';

    const Parse_Error               = '代码语法错误';
    const Reflection_Exception      = '服务器异常映射';
    const Run_Time_Exception        = '服务器运行期异常';
    const Error_Exception           = '框架运行错误';
    const Model_Not_Found_Exception = '数据库模型错误';
    const PDO_Exception             = '数据库链接错误';
    const UNAUTHORIZED              = '未登录或登录超时';
    const UNAUTHORIZED_TWO          = '令牌不正确或已在其他设备登录';

    const TOKEN_ERROR_KEY     = 'apikey错误';   // 70001
    const TOKEN_ERROR_SET     = '请先登录';      // 70002
    const TOKEN_ERROR_BLACK   = 'token 被拉黑';  // 70003
    const TOKEN_ERROR_EXPIRED = 'token 过期';   // 70004
    const TOKEN_ERROR_JWT     = '无效令牌';      //  70005
    const TOKEN_ERROR_JTB     = '令牌缺失';      // 70006
}
