<?php

namespace app\common\exception;

use Throwable;

class ApiException extends \Exception
{
    public function __construct(array $apiErrconst, Throwable $previous = null)
    {
        parent::__construct($apiErrconst['message'], $apiErrconst['status'], $previous);
    }
}
