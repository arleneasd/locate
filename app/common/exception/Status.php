<?php

declare(strict_types=1);

namespace app\common\exception;


class Status
{
    //自定义状态

    const OK                    = 20000;  //  请求成功。一般用于GET与POST请求
    const BAD_REQUEST           = 40000;  //  客户端请求的语法错误，服务器无法理解
    const INTERNAL_SERVER_ERROR = 50000;  //  服务器内部错误，无法完成请求

    const Parse_Error               = 50001;  //  代码语法错误
    const Reflection_Exception      = 50002;  //  服务器异常映射
    const Run_Time_Exception        = 50003;  //  服务器运行期异常
    const Error_Exception           = 50004;  //  框架运行错误
    const Model_Not_Found_Exception = 60000;  //  数据库模型错误
    const PDO_Exception             = 60001;  //  数据库链接错误
    const UNAUTHORIZED              = 40001;  //  请求用户的身份认证失败
    const UNAUTHORIZED_TWO          = 40002;

    const TOKEN_ERROR_KEY     = 70001;
    const TOKEN_ERROR_SET     = 70002;
    const TOKEN_ERROR_BLACK   = 70003;
    const TOKEN_ERROR_EXPIRED = 70004;
    const TOKEN_ERROR_JWT     = 70005;
    const TOKEN_ERROR_JTB     = 70006;
}
