<?php

declare(strict_types=1);

namespace app\common\exception;


class Code
{
    //Http状态码

    const CONTINUE            = 100;    //  继续。客户端应继续其请求
    const SWITCHING_PROTPCOLS = 101;    //  切换协议。服务器根据客户端的请求切换协议。只能切换到更高级的协议，例如，切换到HTTP的新版本协议
    const PROCESSING          = 102;

    const OK                           = 200;  //  请求成功。一般用于GET与POST请求
    const CREATED                      = 201;  //  已创建。成功请求并创建了新的资源
    const ACCEPTED                     = 202;  //  已接受。已经接受请求，但未处理完成
    const NON_AUTHORIATIVE_INFORMATION = 203;  //  返回信息不确定或不完整
    const NO_CONTENT                   = 204;  //  服务器成功处理，但未返回内容
    const RESET_CONTENT                = 205;  //  服务器处理成功，用户终端（例如：浏览器）应重置文档视图
    const PARTIAL_CONTENT              = 206;  //  服务器成功处理了部分GET请求
    const MULTI_STATUS                 = 207;

    const MULTIPLE_CHOICES   = 300;    //   请求的资源可包括多个位置
    const MOVED_PERMANENTLY  = 301;    //   永久移动,删除请求数据
    const FOUND              = 302;    // 	临时移动,在其他地方发现请求数据
    const SEE_OTHER          = 303;    //   建议访问其他URL或者访问方式
    const NOT_MODIFIED       = 304;    //   所请求的资源未修改
    const USER_PROXY         = 305;    //   所请求的资源必须通过代理访问(必须从服务器指定地址获取)
    const UNUSED             = 306;    //   已经被废弃的HTTP状态码
    const TEMPORARY_REDIRECT = 307;    //   申明请求的资源临时性删除

    const BAD_REQUEST                     = 400;    //  客户端请求的语法错误，服务器无法理解
    const UNAUTHORIZED                    = 401;    //  请求用户的身份认证失败
    const PAYMENT_GRANTED                 = 402;    //  保留有效ChargeTo头响应
    const FORBIDDEN                       = 403;    //  服务器理解请求客户端的请求，但是拒绝执行此请求
    const FILE_NOT_FOUND                  = 404;    // 	服务器无法根据客户端的请求找到资源（网页
    const METHOD_NOT_ALLOWED              = 405;    //  客户端在Request-Line请求中定义的方法被禁止
    const NOT_ACCEPTABLE                  = 406;    //  服务器无法根据客户端请求的内容特性完成请求
    const PROXY_AUTHENTICATION_REQUIRED   = 407;    //  请求要求代理的身份认证，与401类似，但请求者应当使用代理进行授权
    const REQUEST_TIME_OUT                = 408;    //  服务器等待客户端发送的请求时间过长，超时
    const CONFLICT                        = 409;    //  服务器完成客户端的 PUT 请求时可能返回此代码，服务器处理请求时发生了冲突
    const GONE                            = 410;    // 	客户端请求的资源已经不存在
    const LENGTH_REQUIRED                 = 411;    //  服务器无法处理客户端发送的不带Content-Length的请求信息
    const PRECONDITION_FAILED             = 412;    //  客户端在当前请求中一个或多个头字段错误
    const REQUEST_ENTITY_TOO_LARGE        = 413;    //  请求资源大于服务器允许大小
    const REQUEST_URL_TOO_LARGE           = 414;    //  请求的URI过长（URI通常为网址），服务器无法处理
    const UNSUPPORTED_MEDIA_TYPE          = 415;    //  服务器无法处理请求附带的媒体格式
    const REQUESTED_RANGE_NOT_SATISFIABLE = 416;    //  客户端请求的范围无效
    const EXPECTATION_FAILED              = 417;    //  服务器无法满足Expect的请求头信息
    const UNPROCESSABLE_ENTITY            = 422;
    const LOCKED                          = 423;
    const FAILED_DEPENDENCY               = 424;

    const INTERNAL_SERVER_ERROR      = 500;     //  服务器内部错误，无法完成请求
    const NOT_IMPLEMENTED            = 501;     //  服务器不支持请求的功能，无法完成请求
    const BAD_GATEWAY                = 502;     //  服务器暂不可用，有时是为了防止发生系统过载
    const SERVICE_UNAVAILABLE        = 503;     //  服务器过载或系统维护
    const GATEWAY_TIMEOUT            = 504;     //  关口过载
    const HTTP_VERSION_NOT_SUPPORTED = 505;     //  服务器不支持请求的HTTP协议的版本，无法完成处理
    const INSUFFICIENT_STORAGE       = 507;
}
