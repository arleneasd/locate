<?php
namespace app\common\Helper;

/**
 * 应用站点工具类
 */
class AppTools
{
    /**
     * @param $list
     * @param $total_num
     * @param $currage
     * @param $pagesize
     * @return array
     * @comment 数据库分页该函数判断组合
     */
    public static function morePage($list,$total_num,$currage,$pagesize){
        $hasmore=false;
        $list = self::_unsetNull($list);
        $total_page = 0;
        if($total_num >0 && $pagesize>0){
            $total_page=ceil($total_num/$pagesize);//总页数
        }
        if($currage<$total_page){
            $hasmore=true;
        }
        return ['hasmore'=>$hasmore,'total_page'=>$total_page,'total_num'=>$total_num,'list'=>$list];
    }

    /**
     * @param $arr
     * @return array|string
     * @comment 递归方式把数组或字符串 null转换为空''字符串
     */
    public static function _unsetNull($arr){
        if($arr !== null){
            if(is_array($arr)){
                if(!empty($arr)){
                    foreach($arr as $key => $value){
                        if($value === null){
                            $arr[$key] = '';
                        }else{
                            $arr[$key] = static::_unsetNull($value);      //递归再去执行
                        }
                    }
                }else{
                    $arr = [];
                }
            }else{
                if($arr === null){
                    $arr = '';
                }         //注意三个等号
            }
        }else{
            $arr = '';
        }
        return $arr;
    }

    /**
     * 禁言属性 转换中文
     * @param int $no_speak_time 禁言分钟
     * @return string
     */
    public static function group_no_speak_time_str($no_speak_time){
        switch ($no_speak_time){
            case 0:
                $no_speak_time_name = '永久';
                break;
            case 10:
                $no_speak_time_name = '10分钟';
                break;
            case 60:
                $no_speak_time_name = '1小时';
                break;
            case 720:
                $no_speak_time_name = '12小时';
                break;
            case 1440:
                $no_speak_time_name = '1天';
                break;
            case 10080:
                $no_speak_time_name = '7天';
                break;
            default:
                if($no_speak_time > 0){
                    $no_speak_time_name = $no_speak_time.'分钟';
                }else{
                    $no_speak_time_name = '正常';
                }
                break;
        }
        return $no_speak_time_name;
    }

    /**
     * 响应格式
     * @param $code
     * @param $message
     * @param $data
     * @return array
     */
    public static function getResponseFormat($code = 0, $message = '', $data = [])
    {
        return ['code' => $code, 'message' => $message, 'data' => $data];
    }

    /**
     * 参数 id 范围
     * @param string $id_range_str 0,100
     * @return array|string[]
     */
    public static function queue_key_id_range($id_range_str){
        $id_range_arr = [];
        if($id_range_str && preg_match('/\d+,\d+/',$id_range_str)){
            $id_range_arr = explode(',',$id_range_str);
            if(isset($id_range_arr[0]) && isset($id_range_arr[1])){
                $id_range_arr[0] = intval($id_range_arr[0]);
                $id_range_arr[1] = intval($id_range_arr[1]);
            }else{
                $id_range_arr = [];
            }
        }
        return $id_range_arr;
    }

    /**
     * 当前时间到次日开始相差时间戳
     * @return int
     */
    public static function thisGoMingInt(){
        $ming_time = strtotime(date('Y-m-d 00:00:00',strtotime('+1 day')));
        $row_res = $ming_time - time();
        if($row_res <= 0){
            $row_res = 0;
        }
        return $row_res;
    }

    /**
     * 唯一uniqid
     * @param $prefix
     * @return string
     */
    public static function getUniqid($prefix=''){
        if($prefix){
            $prefix = $prefix.'_'.date('YmdHis').'_';
        }
        return uniqid($prefix);
    }

}